﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRightSlowly : MonoEx
{

	bool shouldMove = false;
	public float speed = 10.0f;

	void OnEnable()
	{
		AudioRecording.OnReset += Stop;

	}

	void OnDisable()
	{
		AudioRecording.OnReset -= Stop;
	}



	void Update()
	{
		if (!shouldMove)
			return;

		transform.Translate(1 * speed * Time.deltaTime, 0, 0);
	}


	public void Play()
	{
		if (transform.position != GetOrigPos())
			Reset();
		shouldMove = true;
	}

	public void Stop()
	{
		shouldMove = false;
		Reset();
	}
}
