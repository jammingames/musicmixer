﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class GenerateBars : MonoBehaviour
{

	[Range(2, 30)]
	public int numColliders;
	public float xOffset;
	float yOffset;
	public GameObject prefab;
	public Transform parentTransform;
	int previousNumColliders;
	int numBars = 11;

	private List<GameObject> prefabs;

	void Awake()
	{
		previousNumColliders = numColliders;
		prefabs = new List<GameObject>();
	
	}

	//	IEnumerator Start()
	//	{
	//		yield return new WaitForEndOfFrame();
	//		ForceDestroyBars();
	//		yield return new WaitForEndOfFrame();
	//		if (prefab != null && prefabs.Count <= 0)
	//		{
	//			if (transform.childCount < 3)
	//			{
	//
	//				StartCoroutine(GenerateCubes());
	//			}
	//		}
	//	}
	//



	void Update()
	{
		if (prefab == null)
			return;
		
		if (previousNumColliders != numColliders)
		{
			StartCoroutine(GenerateCubes());
		}
		previousNumColliders = numColliders;
	}



	IEnumerator GenerateCubes()
	{
		if (prefab != null)
		{
			
			if (transform.childCount > 3)
				ForceDestroyBars();

			yOffset = GetComponent<RectTransform>().sizeDelta.y / ((float)numBars);
//			print(yOffset + "  " + GetComponent<RectTransform>().sizeDelta.y);

			yield return new WaitForEndOfFrame();
			Color matCol = Color.blue;
			for (int i = 0; i < numBars; i++)
			{
				if (i % 2 == 0)
				{
					matCol = Color.blue;
				}
				else
					matCol = Color.green;
				
				Vector3 spawnPos = Vector3.zero;


				spawnPos.y += (yOffset * i);
				for (int j = 0; j < numColliders; j++)
				{
					string name = i.ToString();
					spawnPos.x = (xOffset * j);
					GameObject gObj = GameObject.Instantiate(prefab, Vector3.zero, parentTransform.rotation, parentTransform);
					prefabs.Add(gObj);
//					name += " " + spawnPos.x;
					gObj.name = name;
					gObj.GetComponent<RectTransform>().anchoredPosition = spawnPos;
					#if UNITY_EDITOR
					matCol.a = 0.5f;
					gObj.GetComponent<Image>().color = matCol;

					#endif
					#if !UNITY_EDITOR 
				
					gObj.GetComponent<Image>().enabled = false;;
					#endif

				
				}

			}
		}
		print("Bars generated");
	}


	public void DestroyBars()
	{
		if (prefabs.Count > 0)
		{
			for (int i = 0; i < prefabs.Count; i++)
			{
				GameObject.DestroyImmediate(prefabs[i]);
			}
			prefabs.Clear();
			prefabs = new List<GameObject>();
			print("Bars destroyed");
		}

	}

	public void ForceDestroyBars()
	{
		
		foreach (Transform child in transform)
		{
			if (child.name.Contains("Cube"))
			{
				print("Destroying " + child.name);
				GameObject.DestroyImmediate(child.gameObject);
			}
		}
	
		prefabs.Clear();
		prefabs = new List<GameObject>();
		print("Bars forcefully destroyed");


	}


}
