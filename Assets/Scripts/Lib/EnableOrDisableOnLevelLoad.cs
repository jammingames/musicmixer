﻿using UnityEngine;
using System.Collections;

public class EnableOrDisableOnLevelLoad : MonoBehaviour
{

	public GameObject[] gObjsToEnable;
	public GameObject[] gObjsToDisable;

	public int desiredLevel;

	void Start ()
	{
		int level = Application.loadedLevel;
//		print (level + " " + desiredLevel);
		bool enable = false;
		if (level == desiredLevel) {
			enable = true;
		} else {
			enable = false;
		}
		if (gObjsToEnable.Length > 0) {
			for (int i = 0; i < gObjsToEnable.Length; i++) {
				gObjsToEnable [i].SetActive (enable);
			}

		}
		if (gObjsToDisable.Length > 0) {
			for (int i = 0; i < gObjsToDisable.Length; i++) {
				gObjsToDisable [i].SetActive (!enable);
			}

		}
	}

	void OnLevelWasLoaded (int level)
	{
		bool enable = false;
		if (level == desiredLevel) {
			enable = true;
		} else {
			enable = false;
		}
		if (gObjsToEnable.Length > 0) {
			for (int i = 0; i < gObjsToEnable.Length; i++) {
				gObjsToEnable [i].SetActive (enable);
			}

		}
		if (gObjsToDisable.Length > 0) {
			for (int i = 0; i < gObjsToDisable.Length; i++) {
				gObjsToDisable [i].SetActive (!enable);
			}

		}
	}

}
