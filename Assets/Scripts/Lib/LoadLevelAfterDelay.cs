﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class LoadLevelAfterDelay : MonoBehaviour
{

	public Graphic img;

	public float fadeDuration = 1;
	public float delay = 5;

	public string nextLevel;

	void Awake ()
	{
		if (img != null) {
			img.SetAlpha (1);
			StartCoroutine (img.FadeAlpha (0, fadeDuration, EaseType.SmoothStepInOut, null));
		}
	}

	IEnumerator Start ()
	{
		yield return new WaitForSeconds (delay + fadeDuration);
		if (img != null) {
			yield return StartCoroutine (img.FadeAlpha (1, fadeDuration, EaseType.SmoothStepInOut, null));
		}
		//AnimationEventSystem.TriggerSceneChange (nextLevel);
	}


}
