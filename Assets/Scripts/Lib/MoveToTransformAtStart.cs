﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveToTransformAtStart : MonoBehaviour
{
	public Transform target;
	public EaseType easer;
	public float duration = 1;
	public float delay = 1;
	public bool moveFrom = false;
	Vector3 origPos;
	Quaternion origRot;

	void Awake ()
	{
		origPos = transform.position;
		origRot = transform.rotation;
	}

	IEnumerator Start ()
	{
		
		if (moveFrom) {
			transform.position = target.position;
			transform.rotation = target.rotation;
			if (delay > 0) {
				yield return new WaitForSeconds (delay);
			}
			StartCoroutine (transform.MoveTo (origPos, duration, easer, null));
			StartCoroutine (transform.RotateTo (origRot, duration, easer, null));
		} else {
			if (delay > 0) {
				yield return new WaitForSeconds (delay);
			}
			StartCoroutine (transform.MoveToTransform (target, duration, easer, null));
		}
	}
}