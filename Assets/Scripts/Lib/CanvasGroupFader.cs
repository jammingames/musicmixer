﻿using UnityEngine;
using System.Collections;


public class CanvasGroupFader : MonoBehaviour
{

	CanvasGroup cGroup;
	public EaseType easer;
	public float duration;
	public float delay;
	public float targetAlpha = 1;
	public bool startInvisible = true;
	public bool fadeOut = false;
	public bool startWithDelay = false;
	[HideInInspector]
	public bool inTransition = false;
	float origAlpha;

	void Awake ()
	{
		cGroup = GetComponent<CanvasGroup> ();
		origAlpha = cGroup.alpha;
		if (startInvisible)
			cGroup.alpha = 0;
	}

	IEnumerator Start ()
	{
		inTransition = true;
		if (startWithDelay) {
			yield return new WaitForSeconds (delay);
		}
		StartCoroutine (cGroup.FadeAlpha (targetAlpha, duration, easer, () => {
			if (fadeOut) {
				StartCoroutine (Auto.Wait (delay, () => {
					StartCoroutine (cGroup.FadeAlpha (0, duration, easer, () => {
						inTransition = false;
					}));
				}));
			}
		}));
		yield return 0;	
	}


}
