﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Instrument : MonoBehaviour
{
	
	public AudioSource audioSource;
	public RawImage img;
	public Image border;
	public int note = 3;
	public bool soFreshSoClean = true;
	public InstrumentButton instrumentBtn;

	float[] pitches = new float[11]{ 0.7f, 0.8f, 0.9f, 1.0f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f };

	Vector3 closestPos;

	private RectTransform t;



	void OnEnable()
	{
		AudioRecording.OnReset += Reset;
		AudioRecording.OnClear += Reset;

	}

	void OnDisable()
	{
		AudioRecording.OnClear -= Reset;
		AudioRecording.OnReset -= Reset;
	}

	void Awake()
	{
		t = GetComponent<RectTransform>();
		audioSource = GetComponent<AudioSource>();
		note = 3;
		audioSource.pitch = pitches[note];
	}

	IEnumerator Start()
	{
		yield return new WaitForSeconds(0.1f);

		yield return new WaitForEndOfFrame();
		if (closestPos != Vector3.zero)
		{
			transform.position = closestPos;
			audioSource.pitch = pitches[note];
		}
		print(note);
		print("setting pitch to " + audioSource.pitch);
	}


	public void Click()
	{
		
		print("CLICKED");
	}

	public void UnClick()
	{
		
		print("DONE CLICKING");
		if (closestPos != Vector3.zero)
		{
			transform.position = closestPos;
			audioSource.pitch = pitches[note];
		}

		if (soFreshSoClean)
		{
			if (instrumentBtn != null)
				instrumentBtn.Spawn();
			soFreshSoClean = false;
		}


			
	}

	public void Reset()
	{
		Destroy(gameObject);
	}


	public void OnDrag()
	{
		transform.position = Input.mousePosition; 
	}

	void OnTriggerEnter(Collider col)
	{
		print("Collided with: " + col.name + "  " + col.transform.position);
		if (col.CompareTag("cube"))
		{
			
			closestPos = col.transform.position;
			note = int.Parse(col.name);
			print("new note is " + note);
		}
		else if (col.CompareTag("line"))
		{
			audioSource.Play();		
		}
//		closestPos = col.GetComponent<RectTransform>().anchoredPosition;
	}

}
