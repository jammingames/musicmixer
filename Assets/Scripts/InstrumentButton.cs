﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstrumentButton : MonoBehaviour
{

	public GameObject prefab;
	public Instrument instrument;
	public RawImage rawImg;
	AudioSource audioSource;
	public Texture2D img;


	public AudioClip clip;
	public Transform parentTransform;

	Button btn;

	void OnEnable()
	{
		AudioRecording.OnReset += Reset;

	}

	void OnDisable()
	{
		AudioRecording.OnReset -= Reset;
	}

	void Awake()
	{
		btn = GetComponent<Button>();
		audioSource = GetComponent<AudioSource>();

	}

	public void Spawn()
	{
		GameObject gObj = GameObject.Instantiate(prefab, transform.position, transform.rotation, parentTransform);
		instrument = gObj.GetComponent<Instrument>();
		instrument.audioSource.clip = clip;
		instrument.img.texture = img;
		instrument.instrumentBtn = this;
		instrument.soFreshSoClean = true;
	}

	public void Create(AudioClip newClip, Texture2D newImg)
	{
		img = newImg;
		clip = newClip;

		audioSource.clip = newClip;

		rawImg.texture = newImg;
		Spawn();
	}

	public void Reset()
	{
		rawImg.texture = null;
		clip = null;
		img = null;
		instrument = null;
	}

}
