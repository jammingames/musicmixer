﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AudioRecording : Singleton<AudioRecording>
{
	
	/*
	 * Things needed:
	 * 
	 * play/pause/stop/record
	 * place sounds
	 * delete sounds
	 * clear all/ clear all but keep sounds
	 * 
	 * record sounds and picture
	 * store recorded sound and picture in a singular object class with a border
	 * add border image to stamp object
	 * 
	 * populate bottom of screen with said stamps
	 * 
	 * adjust pitch of sound based on where on board it is placed
	 * 
	 * 
	 * doot doot
	 * 
	 * 
	 * pop up  with 3 . 2. 1 for recording and some sort of indication.
	 * 
	 * ability to generate dumb art from a list in the absence of camera functionality
	 * 
	 * Dank memes for said list
	 * 
	 * Dank memes for self
	 * 
	 * 
	 * record button will pop up and play webcam texture, with another record button,
	 * when hitting the second record button will be prompted 3 2 1 before it records/takes photo 
	 * asks if save photo/instrument or discard and close popup
	 * 
	 * 
	 * 
	 * */

	public delegate void GameEvent();

	public static event GameEvent OnReset, OnClear;

	public void TriggerReset()
	{
		if (OnReset != null)
		{
			OnReset();
			Reset();
		}
	}

	public void TriggerClear()
	{
		if (OnClear != null)
		{
			OnClear();
		}
	}


	public GameObject instrumentPrefab;
	public Transform parentTransform;

	public AudioSource audioSource;

	public RawImage rawImg;
	public Image maskImg;

	private List<InstrumentButton> _openSpots;

	public List<InstrumentButton> OpenSpots;

	Instrument currentInstrument = null;
	WebCamTexture webCamTexture;

	RectTransform r;
	float origWidth;
	float origHeight;

	void Awake()
	{
		if (maskImg == null)
			maskImg = GetComponent<Image>();
		maskImg.enabled = false;
		rawImg.enabled = false;
		_openSpots = OpenSpots;
		r = rawImg.GetComponent<RectTransform>();
		origWidth = r.sizeDelta.x;
		origHeight = r.sizeDelta.y;
	}


	void Start()
	{
//		openSpots = new List<InstrumentButton>(FindObjectsOfType<InstrumentButton>());

	}

	public void DoRecording()
	{
		StartWebcam();
		StartCoroutine(Record(1));
	}

	public void  StartWebcam()
	{
		if (_openSpots.Count > 0)
		{
			if (r.sizeDelta.x != origWidth || r.sizeDelta.y != origHeight)
				r.sizeDelta = new Vector2(origWidth, origHeight);
//				r.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, origWidth);

//				r.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, origHeight);
//		audioSource = GetComponent<AudioSource>();
			maskImg.enabled = true;
			rawImg.enabled = true;
			webCamTexture = new WebCamTexture();
			webCamTexture.Play();
			rawImg.material.mainTexture = webCamTexture;
			audioSource.clip = null;
		}
	}

	public void  StopWebcam()
	{
		

		//		audioSource = GetComponent<AudioSource>();
		webCamTexture.Stop();
		rawImg.material.mainTexture = null;
		rawImg.texture = null;
		maskImg.enabled = false;
		rawImg.enabled = false;
//			webCamTexture = new WebCamTexture();

	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			if (_openSpots.Count > 0)
			{
				
				StartWebcam();
				StartCoroutine(Record(1));
			}
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			GetComponent<AudioSource>().Play();
		}


	}

	IEnumerator Record(int duration)
	{
		if (_openSpots.Count > 0)
		{
			
			if (!webCamTexture.isPlaying)
			{
			
				webCamTexture.Play();
			}
			yield return new WaitForSeconds(0.2f);
			audioSource.clip = Microphone.Start(null, true, duration, 44100);
			AudioClip newClip = audioSource.clip;
			yield return StartCoroutine(Auto.Wait(duration + 0.05f));

			yield return new WaitForEndOfFrame(); 
//			webCamTexture.Stop();
			Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
			photo.SetPixels(webCamTexture.GetPixels());
			//		print(photo.width + "   " + photo.height);
			photo.Apply();
			if (webCamTexture.isPlaying)
				webCamTexture.Stop();

			//		GetComponent<RawImage>().material.mainTexture = photo;

			Rect r = new Rect((photo.width / 2) - photo.height / 4, photo.height / 4, photo.height / 2, photo.height / 2);
			Texture2D result = new Texture2D((int)r.width, (int)r.height);
			if (r.width != 0 && r.height != 0)
			{
			
				result.SetPixels(photo.GetPixels(Mathf.FloorToInt(r.x), Mathf.FloorToInt(r.y), Mathf.FloorToInt(r.width), Mathf.FloorToInt(r.height)));
				result.Apply();


				rawImg.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, result.width);
				rawImg.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, result.height);
				rawImg.material.mainTexture = null;
				rawImg.texture = result;

			}
		
			Microphone.End(null);
			yield return new WaitForEndOfFrame();
			CreateInstrument(newClip, result);
			StopWebcam();
		}
	}

	void CreateInstrument(AudioClip newClip, Texture2D newImg)
	{
		if (_openSpots.Count > 0)
		{
			_openSpots[0].Create(newClip, newImg);
			_openSpots.RemoveAt(0);
		}
	}



	public void Reset()
	{
		_openSpots = OpenSpots;
		foreach (InstrumentButton btn in _openSpots)
		{
			btn.Reset();
		}
	}
		
}
