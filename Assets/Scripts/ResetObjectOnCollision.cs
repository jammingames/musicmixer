﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ResetObjectOnCollision : MonoBehaviour
{


	void OnTriggerEnter(Collider col)
	{
		if (col.GetComponent<IResettable>() != null)
		{
			col.GetComponent<IResettable>().Reset();
		}
	}
}
